# Obtener los Query Plans del Optimizer de un Dashboard
Este código forma parte de la entrada [Optimización de Queries en Salesforce (Parte 2) – con Query Plan y Tooling/REST API](https://forcegraells.com/2018/10/17/optimizacion-de-queries-en-salesforce-parte-2-evaluar-planes-de-ejecucion-con-query-plan-y-tooling-rest-api/).

Este código consiste en obtener todos los **Query Plans** de los Reports que contiene cada Dashboard Component de un Dashboard de Salesforce. Para ello se invoca el recurso **explain** de la Tooling API.

Consulta el enlace anterior para una descripción completa.

## Licencia
Puedes usar este código bajo la licencia MIT, sin restricciones.